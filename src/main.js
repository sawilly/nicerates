import Vue from 'vue'
import App from './App'
import router from './router'

//Including Vuetify In Project
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
//Importing Material Design Fonts
import 'material-design-icons-iconfont/dist/material-design-icons.css'
 
Vue.use(Vuetify, {
  iconfont: 'mdi'
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
