import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import About from '@/components/About'
import Terms from '@/components/Terms'
import Category from '@/components/Category'
import NotFound from '@/components/NotFound'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/category',
      name: 'Category',
      component: Category
    },
    {
      path: '/terms',
      name: 'Terms',
      component: Terms
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound
    }
  ]
})
